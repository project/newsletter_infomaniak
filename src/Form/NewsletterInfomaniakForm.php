<?php

namespace Drupal\newsletter_infomaniak\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\newsletter_infomaniak\Service\NewsletterInfomaniak;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class NewsletterForm.
 *
 * @package Drupal\newsletter_infomaniak\Form
 */
class NewsletterInfomaniakForm extends FormBase {
  /**
   * Drupal\newsletter_infomaniak\Service\NewsletterInfomaniak definition.
   *
   * @var \Drupal\newsletter_infomaniak\Service\NewsletterInfomaniak
   */
  protected $newsletter;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a \Drupal\demo\Form\Multistep\MultistepFormBase.
   *
   * @param \Drupal\newsletter_infomaniak\Service\NewsletterInfomaniak $newsletter
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   */
  public function __construct(NewsletterInfomaniak $newsletter, MessengerInterface $messenger) {
    $this->newsletter = $newsletter;
    $this->messenger = $messenger;
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('newsletter_infomaniak.manager'),
          $container->get('messenger')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'newsletter_infomaniak_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['email'] = [
      '#title'      => $this->t('Newsletter'),
      '#attributes' => ['placeholder' => $this->t('Mail')],
      '#type'       => 'textfield',
    ];

    $form['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('OK'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Send value.
    $mail = $form_state->getValue('email');
    $success = $this->newsletter->add($mail);

    // Success / or not.
    if ($success) {
      $this->messenger->addMessage($this->t('Your email address has been registered'));
    }
    else {
      $this->messenger->addError($this->t('An error occurred while registering'));
    }

    // Redirect to home.
    $form_state->setRedirect('<front>');
    return;
  }

}
