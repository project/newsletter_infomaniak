<?php

namespace Drupal\newsletter_infomaniak\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\newsletter_infomaniak\Service\NewsletterInfomaniakInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a form that configures forms newsletter settings.
 */
class NewsletterInfomaniakConfigForm extends ConfigFormBase {

  // Const settings.
  const SETTINGS = 'newsletter_infomaniak.configuration';

  /**
   * Drupal\newsletter_infomaniak\Service\NewsletterInfomaniakInterface definition.
   *
   * @var \Drupal\newsletter_infomaniak\Service\NewsletterInfomaniakInterface
   */
  protected $newsletter;

  /**
   * Constructs a \Drupal\demo\Form\Multistep\MultistepFormBase.
   *
   * @param \Drupal\newsletter_infomaniak\Service\NewsletterInfomaniakInterface $newsletter
   */
  public function __construct(NewsletterInfomaniakInterface $newsletter) {
    $this->newsletter = $newsletter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('newsletter_infomaniak.manager')
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'newsletter_infomaniak_form_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {

    // Config default value.
    $config = $this->config(static::SETTINGS);

    // Api key.
    $form['infomaniak_api_key'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Infomaniak API key'),
      '#default_value' => $config->get('infomaniak_api_key'),
    ];

    // Api secret.
    $form['infomaniak_api_secret'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Infomaniak API secret'),
      '#default_value' => $config->get('infomaniak_api_secret'),
    ];

    // Mailing list.
    $options = $this->newsletter->getMailinglists();
    if (count($options) > 0) {
      $form['infomaniak_list_id'] = [
        '#type' => 'select',
        '#title' => $this->t('Infomaniak list ID'),
        '#options' => $this->newsletter->getMailinglists(),
        '#default_value' => $config->get('infomaniak_list_id'),
      ];
    }

    // Return form.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get all values.
    $values = $form_state->getValues();

    // Default value.
    if (!isset($values['infomaniak_list_id'])) {
      $values['infomaniak_list_id'] = '';
    }

    // Save.
    $this->config(static::SETTINGS)
      ->set('infomaniak_api_key', $values['infomaniak_api_key'])
      ->set('infomaniak_api_secret', $values['infomaniak_api_secret'])
      ->set('infomaniak_list_id', $values['infomaniak_list_id'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
