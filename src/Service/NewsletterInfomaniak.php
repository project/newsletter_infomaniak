<?php

namespace Drupal\newsletter_infomaniak\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\newsletter_infomaniak\Form\NewsletterInfomaniakConfigForm;
use Infomaniak\ClientApiNewsletter\Action;
use Infomaniak\ClientApiNewsletter\Client;

/**
 * NewsletterInfomaniak.
 *
 * @package Drupal\newsletter_infomaniak\Service
 */
class NewsletterInfomaniak implements NewsletterInfomaniakInterface {
  /**
   * Default_list_id.
   *
   * @var int
   */
  private $default_list_id;

  /**
   * ClientApiNewsletter.
   *
   * @var class
   */
  private $client;

  /**
   * LoggerChannelFactoryInterface.
   *
   * @var class
   */
  private $logger_factory;

  /**
   * Client NewsletterInfomaniak.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   */
  public function __construct(ConfigFactoryInterface $configFactory, LoggerChannelFactoryInterface $logger_factory) {
    // Logger.
    $this->logger_factory = $logger_factory;

    // Config.
    $config = $configFactory->get(NewsletterInfomaniakConfigForm::SETTINGS);

    // Client.
    $this->client = new Client($config->get('infomaniak_api_key'), $config->get('infomaniak_api_secret'));

    // Default _list_id.
    $this->default_list_id = $config->get('infomaniak_list_id');
  }

  /**
   * Add email inside specific list.
   *
   * @param string $mail
   *   The mail value.
   * @param array $merge_fields
   *   Multilple fields.
   * @param string $list_id
   *   Optional list_id.
   *
   * @return bool
   *   Return TRUE if the subscribition is OK.
   *
   * @throws \Exception
   *    Is throws if the script have an error.
   */
  public function add($mail, $merge_fields = [], $list_id = NULL) {
    try {
      // Email.
      $merge_fields['email'] = $mail;

      // Send client.
      $response = $this->client->post(
            Client::MAILINGLIST, [
              'id'        => (!is_null($list_id)) ? $list_id : $this->default_list_id ,
              'action'    => Action::IMPORTCONTACT,
              'params'    => ['contacts' => $merge_fields],
            ]
        );

      // Return response.
      return $response->success();
    }
    catch (\Exception $e) {
      $this->logger_factory->get('newsletter_infomaniak')->error(
            'An error occurred requesting add information from newsletter_infomaniak. "{message}"', [
              'message' => $e->getMessage(),
            ]
        );
    }
  }

  /**
   * Remove email from specific list.
   *
   * @param string $mail
   *   The mail value.
   * @param string $list_id
   *   Optional list_id.
   *
   * @return bool
   *   Return TRUE if the unsubscribition is OK.
   *
   * @throws \Exception
   *    Is throws if the script have an error.
   */
  public function remove($mail, $list_id = NULL) {
    try {
      // Send client.
      $response = $this->client->post(
            Client::MAILINGLIST, [
              'id'     => (!is_null($list_id)) ? $list_id : $this->default_list_id,
              'action' => Action::MANAGECONTACT,
              'params' => [
                'email'  => $mail,
                'status' => Action::DELETE,
              ],
            ]
        );

      // Return response.
      return $response->success();
    }
    catch (\Exception $e) {
      $this->logger_factory->get('newsletter_infomaniak')->error(
            'An error occurred requesting remove information from newsletter_infomaniak. "{message}"', [
              'message' => $e->getMessage(),
            ]
        );
    }
  }

  /**
   * Retrieve all mailinglists.
   *
   * @param int $page
   *   optional current page.
   * @param int $per_page
   *   number of items per page.
   *
   * @return array
   *   Return all mailinglists.
   *
   * @throws \Exception
   *    Is throws if the script have an error.
   */
  public function getMailinglists($page = 1, $per_page = 200) {
    try {
      $response = $this->client->get(
            Client::MAILINGLIST, [
              'params' => [
                'page' => $page,
                'perPage' => $per_page,
              ],
            ]
        );
      $list = [];
      if ($response->success()) {
        $data = $response->datas();
        if ($data['total'] > 0) {
          foreach ($data['data'] as $item) {
            $list[$item['id']] = '[' . $item['id'] . '] ' . $item['name'];
          }
        }
      }

      // Return list.
      return $list;
    }
    catch (\Exception $e) {
      $this->logger_factory->get('newsletter_infomaniak')->error(
            'An error occurred requesting getMailinglists information from newsletter_infomaniak. "{message}"', [
              'message' => $e->getMessage(),
            ]
        );
    }
  }

}
