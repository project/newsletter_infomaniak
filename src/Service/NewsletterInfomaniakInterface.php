<?php

namespace Drupal\newsletter_infomaniak\Service;

/**
 * NewsletterInfomaniakInterface.
 *
 * @package Drupal\newsletter_infomaniak\Service
 */
interface NewsletterInfomaniakInterface {

  /**
   * Add email inside specific list.
   *
   * @param string $mail
   *   The mail value.
   * @param array $merge_fields
   *   Multilple fields.
   * @param string $list_id
   *   Optional list_id.
   *
   * @return bool
   *   Return TRUE if the subscribition is OK.
   *
   * @throws \Exception
   *    Is throws if the script have an error.
   */
  public function add($mail, $merge_fields = [], $list_id = NULL);

  /**
   * Remove email from specific list.
   *
   * @param string $mail
   *   The mail value.
   * @param string $list_id
   *   Optional list_id.
   *
   * @return bool
   *   Return TRUE if the unsubscribition is OK.
   *
   * @throws \Exception
   *    Is throws if the script have an error.
   */
  public function remove($mail, $list_id = NULL);

  /**
   * Retrieve all mailinglists.
   *
   * @param int $page
   *   optional current page.
   * @param int $per_page
   *   number of items per page.
   *
   * @return array
   *   Return all mailinglists.
   *
   * @throws \Exception
   *    Is throws if the script have an error.
   */
  public function getMailinglists($page = 1, $per_page = 200);

}
