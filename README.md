# NEWSLETTER INFOMANIAK

This module is an integration with the [Infomaniak's newsletter tool](https://www.infomaniak.com/en/marketing-events/newsletter-tool).

## Dependencies

The Drupal 8/9 version of Newsletter infomaniak require "infomaniak/client-api-newsletter" !

## Getting Started

Install the module with `composer`.

```bash
$ composer require 'drupal/newsletter_infomaniak:^1.0'
```

Don't forget to enable the modules and configure it !

You must specific 2 fields inside the configuration (admin/config/newsletter_infomaniak) : 

- `Infomaniak API key` string - Your API KEY on the infomaniak newsletter tool.
- `Infomaniak API secret` string - Your API SECRET on the infomaniak newsletter tool.

## How to use ?

**Subscription**


```php
\Drupal::service('newsletter_infomaniak.manager')->add($mail, $merge_fields, $list_id)
```

- `$mail` string - The mail value
- `$merge_fields` array (optional) - Multilple fields 
- `$list_id` string (optional) - Optional default list id

**Unsubscription**

```php
\Drupal::service('newsletter_infomaniak.manager')->remove($mail, $list_id)
```

- `$mail` string - The mail value
- `$list_id` string (optional) - Optional default list id


